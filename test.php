<?php

class YaSmsParser
{
    const FIELD_SUM = 'sum';
    const FIELD_CODE = 'code';
    const FIELD_ACCOUNT = 'account';

    /**
     * Правила парсинга сообщения. Правила имеют приоритет, по которому они сортируются и используются в указанном порядке.
     * Суть этого в том, чтобы в первую очередь определить и убрать из сообщения данные, которые идентифицированы с
     * наибольшей вероятностью правильно, тогда менее точные правила подхватят данные из оставшегося тела сообщения.
     *
     * @var array
     */
    protected $rules = [
        // В первую очередь будут идти проверки на номер кошелька, т.к. его вычленить из текста можно с наибольшей вероятностью
        [
            'field' => self::FIELD_ACCOUNT,
            'priority' => 10000,
            'mask' => '/(41001\d{8, 10})/si',
        ],
        [
            'field' => self::FIELD_ACCOUNT,
            'priority' => 10001,
            'mask' => '/Перевод на счет (41001\d{8,10})/si',
        ],

        // Далее проверки на сумму
        [
            'field' => self::FIELD_SUM,
            'priority' => 1009,
            'mask' => '/Спишется (\d+,?(\d{2})?)р\./si',
        ],
        [
            'field' => self::FIELD_SUM,
            'priority' => 1000,
            'mask' => '/(\d+,?(\d{1,2})?)р\./si',
        ],
        [
            'field' => self::FIELD_SUM,
            'priority' => 1005,
            // Даже если нет указания валюты, то число с зяпятой наверняка является суммой - в кошельке и коде ее быть не может
            'mask' => '/(\d+,\d{1,2})/si',
        ],

        // В последнюю очередь пробуем найти код
        [
            'field' => self::FIELD_CODE,
            'priority' => 1, // Кейс тоже подходит, т.к. после отработки предыдущих правил в тексте не должно остаться чисел
            'mask' => '/(\d{4,})/si',
        ],
        [
            'field' => self::FIELD_CODE,
            'priority' => 9, // Наиболее точный кейс
            'mask' => '/Пароль: (\d{4,})/si',
        ],

    ];

    /**
     * Алгоритм парсинга следующий:
     *      - Сортируем правила по приоритету
     *      - Пытаемся выполнить каждое правило, если оно выполняется - заполняем соответствующее поле результата
     *      - Проводим проверку, определились ли все необходимые данные
     *
     * @param string $str
     * @param integer $iterateCount
     * @return array
     * @throws Exception
     */
    public function parse($str, $iterateCount = 1)
    {
        $result = [];

        // Сортируем правила по приоритету
        usort($this->rules, function ($rule1, $rule2) {
            if ($rule1['priority'] == $rule2['priority']) {
                return 0;
            }

            return $rule1['priority'] > $rule2['priority'] ? -1 : 1;
        });

        // Суть этого цикла в том, чтобы в случае, если под какое-то правило попадают несколько вариантов данных,
        // то дать возможность другим правилам более точно определить содержимое строки. Т.е. если, к примеру,
        // по маске кода попадет сам код и номер аккаунта (это возможно в теории, если выставить правила специальным образом),
        // то в этом случае скрипт проведет еще одну итерацию, найдет более точные данные, вырежит их, и тогда определение
        // кода сработает однозначно.
        for ($i = 0; $i < $iterateCount; $i++) {
            $result = $this->processRules($str, $result);
        }

        // Успешный результат возможен только если все необходимые поля заполнены
        return count($result) === 3 ? $result : null;
    }

    /**
     * Перебирает правила, указанные вверху скрипта и по ним пытается заполнить массив $result необходимыми полями
     *
     * @param string $str
     * @param array $result
     * @return array
     */
    protected function processRules(&$str, $result)
    {
        foreach ($this->rules as $rule) {

            // Если поле уже заполнено - пропускаем правило
            if (isset($result[$rule['field']])) {
                continue;
            }

            if (preg_match_all($rule['mask'], $str, $matches)) {
                // Правило считается отработавшим, если всего одно совпадение.
                // Если совпадений больше - значит условие не достаточно точное
                if (count($matches[1]) == 1) {
                    $result[$rule['field']] = $matches[1][0];
                    // Считается, что первые правила более точно определяют данные, поэтому очистим строку от того,
                    // что определилось однозначно
                    $str = str_replace($matches[0][0], '', $str);
                }
            }
        }

        return $result;
    }
}

$parser = new YaSmsParser();

$testCount = 1000;
for ($i = 0; $i < $testCount; $i++) {
    echo sprintf('Test %s of %s', $i + 1, $testCount) . "\n";
    $sum = rand(1, 995000) / 100;
    $account = 41001 . str_pad(rand(0, 999999), rand(8, 10), '0', STR_PAD_LEFT);

    $response = getTestData($account, $sum);

    if (empty($response)) {
        echo sprintf('Empty response for account "%s" and sum "%s"', $account, $sum) . "\n";
        return;
    }

    $parseResult = $parser->parse($response);

    if (empty($parseResult)) {
        echo sprintf('Empty parse result for account "%s" and sum "%s", emulator response: "%s"', $account, $sum, $response) . "\n";
        return;
    }

    echo sprintf('Success parse for response "%s". Result: %s', $response, print_r($parseResult, true)) . "\n";
}

function getTestData($account, $sum)
{
    $url = 'https://funpay.ru/yandex/emulator';
    $data = array('receiver' => $account, 'sum' => $sum);

    $fields_string = http_build_query($data);


    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_POST,count($data));
    curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Requested-With: XMLHttpRequest"));
    curl_setopt($ch,CURLOPT_HEADER, false);

    return curl_exec($ch);
}